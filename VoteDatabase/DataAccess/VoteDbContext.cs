﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.Models.BO;

// Ignore spelling: bürger Behörde Winsen
namespace VoteDatabase.DataAccess
{
    /// <summary>
    /// Default local:
    /// SET default_storage_engine = 'InnoDB';
    ///
    /// Default bürger-stimme.de
    /// SET default_storage_engine = 'MyISAM';
    /// </summary>
    public class VoteDbContext : IdentityDbContext<User>
    {
        public VoteDbContext(DbContextOptions<VoteDbContext> options)
            : base(options)
        {
            // Disable lazy loading so we have control about database calls
            ChangeTracker.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            if (builder == null) { throw new ArgumentNullException(nameof(builder)); }
            base.OnModelCreating(builder);

            builder.HasDefaultSchema("Vote");
            CreateAuthenticationTables(builder);

            builder.Entity<BCase_Institution>().HasKey(sc => new { sc.FkBCase, sc.FkInstitution });

            builder.Entity<Valuation>()
                .HasOne(p => p.CaseInstitution)
                .WithMany(b => b!.Valuations)
                .HasForeignKey(b => b.FkCaseInstitution)
                .HasPrincipalKey(b => b!.Id);

            SeedSampleData(builder);
        }

        private static void SeedSampleData(ModelBuilder builder)
        {
            var user = new User()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "first@user.com",
                Email = "first@user.com",
                CustomTag2 = "tag",
                LockoutEnd = DateTime.UtcNow,
                NormalizedEmail = "FIRST@USER.COM",
                NormalizedUserName = "FIRST@USER.COM",
                EmailConfirmed = true,
                LockoutEnabled = true,
                PasswordHash = "a", // does not make sense but avoids analyzer warning in generated code
                PhoneNumber = "000-123456789", // avoid analyzer warning in generated code
            };
            builder.Entity<User>().HasData(user);
            var bCase = new BCase("Well done", user.Id)
            {
                Id = 1,
                Category = Models.Enums.Category.Praise,
                Summary = "Well done",
                CreationDate = DateTime.UtcNow,
                //Events = new List<BEvent>(),
                //Institutions =
            };

            var institutionBranch = new InstitutionBranch("Behörde") { Id = 1 };
            builder.Entity<InstitutionBranch>().HasData(institutionBranch);

            var institutionType = new InstitutionType("Super-Amt")
            {
                Id = 1,
                FkBranch = institutionBranch.Id,
            };
            builder.Entity<InstitutionType>().HasData(institutionType);

            var institution = new Institution("Super-Amt Winsen")
            {
                Id = 1,
                FkInstitutionType = institutionType.Id,
                PLZ = 12345,
            };
            builder.Entity<Institution>().HasData(institution);

            // bCase and institution navigations must not bet set for seeding to work.
            // Set FkBCase and FkInstitution instead.
            var bCaseInstitution = new BCase_Institution(null!, null!)
            {
                Id = 1,
                FkInstitution = 1,
                FkBCase = 1,
            };

            var valuation = new Valuation("Optimal", user.Id, bCaseInstitution.Id,
                Models.Enums.ValuationRating.DecisionCorrect)
            {
                Id = 1,
                FkCaseInstitution = bCaseInstitution.Id,
            };

            var bEvent = new BEvent("Event summary", "Event text")
            {
                Id = 1,
                FkInstitution = institution.Id,
                FkBCase = bCase.Id,
                AttachmentFile = "file.txt",
            };
            var bEvent2 = new BEvent("Event summary2", "Second Event text")
            {
                Id = 2,
                FkInstitution = institution.Id,
                FkBCase = bCase.Id,
                AttachmentFile = "file2.txt",
            };
            //bCase.Events.Add(bEvent);

            builder.Entity<Valuation>().HasData(valuation);
            builder.Entity<BEvent>().HasData(bEvent);
            builder.Entity<BEvent>().HasData(bEvent2);
            builder.Entity<BCase_Institution>().HasData(bCaseInstitution);
            builder.Entity<BCase>().HasData(bCase);
        }

        private static void CreateAuthenticationTables(ModelBuilder builder)
        {
            const string IdentitySchema = "Identity";
            builder.Entity<IdentityRole>(entity =>
            {
                entity.Property(i => i.Id).HasMaxLength(100);
                entity.Property(i => i.NormalizedName).HasMaxLength(100);
                entity.ToTable("Role", IdentitySchema);
                entity.HasKey(u => u.Id);
            });
            builder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.Property(i => i.Id).HasMaxLength(100);
                entity.Property(i => i.RoleId).HasMaxLength(100);
                entity.Property(i => i.RoleId).HasColumnName("FkRole");
                entity.ToTable("RoleClaim", IdentitySchema);
            });
            builder.Entity<User>(entity =>
            {
                entity.Property(i => i.NormalizedEmail).HasMaxLength(100);
                entity.Property(i => i.NormalizedUserName).HasMaxLength(100);
                entity.Property(i => i.Id).HasMaxLength(100);
                entity.ToTable("User", IdentitySchema);
                entity.HasKey(u => u.Id);
            });
            builder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.Property(i => i.Id).HasMaxLength(100);
                entity.Property(i => i.UserId).HasMaxLength(100);
                entity.Property(i => i.UserId).HasColumnName("FkUser");
                entity.ToTable("UserClaim", IdentitySchema);
            });
            builder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.Property(i => i.UserId).HasMaxLength(100);
                entity.Property(i => i.LoginProvider).HasMaxLength(100);
                entity.Property(i => i.ProviderKey).HasMaxLength(100);
                entity.Property(i => i.UserId).HasColumnName("FkUser");
                entity.ToTable("UserLogin", IdentitySchema).HasKey(fu => new { fu.LoginProvider, fu.ProviderKey });
            });
            builder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.Property(i => i.RoleId).HasMaxLength(100);
                entity.Property(i => i.UserId).HasMaxLength(100);
                entity.Property(i => i.UserId).HasColumnName("FkUser");
                entity.Property(i => i.RoleId).HasColumnName("FkRole");
                entity.ToTable("User_Role", IdentitySchema).HasKey(fu => new { fu.UserId, fu.RoleId });
            });
            builder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.Property(i => i.UserId).HasMaxLength(100).HasColumnName("FkUser");
                entity.Property(i => i.LoginProvider).HasMaxLength(100);
                entity.Property(i => i.Name).HasMaxLength(50);
                entity.ToTable("UserToken", IdentitySchema);
            });
        }

        public DbSet<BCase> BCase => Set<BCase>();
        public DbSet<BEvent> BEvent => Set<BEvent>();
        public DbSet<HtmlBlock> HtmlBlock => Set<HtmlBlock>();
        public DbSet<Institution> Institution => Set<Institution>();
        public DbSet<InstitutionBranch> InstitutionBranch => Set<InstitutionBranch>();
        public DbSet<InstitutionType> InstitutionType => Set<InstitutionType>();
        public DbSet<User> User => Set<User>();
        public DbSet<Valuation> Valuation => Set<Valuation>();
        public DbSet<WebRequest> WebRequest => Set<WebRequest>();
    }
}
