﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoteDatabase.Constants
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string PiranhaAdministrator = "PiranhaAdministrator";
    }
}
