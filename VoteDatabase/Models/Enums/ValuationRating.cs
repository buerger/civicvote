﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDatabase.Models.Enums
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1028:Enumerationsspeicher muss Int32 sein", Justification = "<Ausstehend>")]
    public enum ValuationRating : byte
    {
        /// <summary>
        /// Not set
        /// </summary>
        None = 0,

        /// <summary>
        /// Decision taken by institution correct
        /// </summary>
        DecisionCorrect = 1,

        /// <summary>
        /// Decision taken by institution wrong
        /// </summary>
        DecisionWrong = 101,

        /// <summary>
        /// User could not decide whether decision taken by institute is correct or not
        /// </summary>
        Undecided = 201,
    }
}
