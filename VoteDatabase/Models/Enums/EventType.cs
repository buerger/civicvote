﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDatabase.Models.Enums
{
    /// <summary>
    /// Type of event (call, letter, ...)
    /// </summary>
    public enum EventType
    {
        /// <summary>
        /// Not set.
        /// </summary>
        None = 0,

        /// <summary>
        /// Letter from/to institution
        /// </summary>
        Letter = 1,

        /// <summary>
        /// Telephone call
        /// </summary>
        Call = 2,

        /// <summary>
        /// Personal contact
        /// </summary>
        Personal = 3,
    }
}
