﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDatabase.Models.Enums
{
    /// <summary>
    /// Category of case (professional problem, communication problem, ...)
    /// </summary>
    public enum Category
    {
        /// <summary>
        /// Not set
        /// </summary>
        None,

        /// <summary>
        /// Decision taken by institution potentially wrong
        /// Fachliches, inhaltliches Problem
        /// </summary>
        Decision = 1,

        /// <summary>
        /// Communication with institution problematic
        /// Formelles Problem
        /// </summary>
        Communication = 2,

        /// <summary>
        /// Case/Event/Decision formally correct, but based on law that does not make sense
        /// </summary>
        Law = 3,

        /// <summary>
        /// Case/Event/Decision/Communication perfect
        /// </summary>
        Praise = 101,
    }
}
