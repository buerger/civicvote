﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDatabase.Models.BO
{
    /// <summary>
    /// Behörde (Typen: KFZ, Meldeamt, Finanzamt...)
    /// Ausbildung (Typen: Grundschule, Uni, ...)
    /// Krankenkasse (Typ: Krankenkasse)
    /// Krankenhaus (Typ: Krankenhaus)
    /// </summary>
    public class InstitutionBranch
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized.")]
        public InstitutionBranch()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }

        public InstitutionBranch(string name)
        {
            Name = name;
        }

        [Key(), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public uint Id { get; set; }
        public string Name { get; set; }
    }
}
