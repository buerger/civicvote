﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace VoteDatabase.Models.BO
{
    public class User : IdentityUser, IEquatable<User>
    {
        public string? CustomTag2 { get; set; }

        [NotMapped]
#pragma warning disable CA2227 // Collection properties should be read only
        public IList<string>? Roles { get; set; }
#pragma warning restore CA2227 // Collection properties should be read only

        public bool Equals(User? other)
        {
            return Id == other?.Id;
        }
    }
}
