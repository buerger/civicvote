﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VoteDatabase.Models.BO
{
    /// <summary>
    /// KFZ Winsen, Meldeamt Seevetal, Finanzamt Buchholz
    /// Grundschule Hittfeld, Uni Hamburg
    /// Krankenhaus Köln, Krankenkase Techniker
    /// ...
    /// </summary>
    public class Institution
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized.")]
        public Institution()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }

        [Key]
        public uint Id { get; set; }

        [ForeignKey(nameof(InstitutionType))]
        public uint FkInstitutionType { get; set; }
        public virtual InstitutionType? InstitutionType { get; set; }
        public string Name { get; set; }

        public Institution(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Use special type ZipCode?
        /// </summary>
        public ushort PLZ { get; set; }

        public virtual ICollection<BCase_Institution>? BCases { get; private set; }
    }
}
