﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VoteDatabase.Models.Enums;

namespace VoteDatabase.Models.BO
{
    /// <summary>
    /// Akte. Fasst alle Ereignisse zu einer Behörden-Kommunikation zusammen.
    /// </summary>
    public class BCase
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized.")]
        public BCase()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }

        public BCase(string summary, string fkUserCreator)
        {
            Summary = summary;
            Institutions = new List<BCase_Institution>();
            FkCreator = fkUserCreator;
            Events = new List<BEvent>();
        }

        [Key(), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public uint Id { get; set; }

        /// <summary>
        /// Date of creation of this case
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Events belonging to this case
        /// </summary>
        public ICollection<BEvent> Events { get; private set; }

        /// <summary>
        /// Involved institutions in this case
        /// </summary>
        public ICollection<BCase_Institution> Institutions { get; private set; }

        /// <summary>
        /// Summary of this case provided by <see cref="Creator"/>
        /// </summary>
        [Column(TypeName = "VARCHAR")]
        [StringLength(1024)]
        public string Summary { get; set; }

        [ForeignKey(nameof(Creator))]
        public string FkCreator { get; set; }

        /// <summary>
        /// User that created the case
        /// </summary>
        public virtual User? Creator { get; set; }

        /// <summary>
        /// Category of case (Type of problem)
        /// </summary>
        public Category Category { get; set; }
    }
}
