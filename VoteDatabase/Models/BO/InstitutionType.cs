﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDatabase.Models.BO
{
    /// <summary>
    /// KFZ Harburg, Meldeamt Seevetal, Finanzamt Buchholz
    /// Grundschule Hittfeld, Uni Hamburg
    /// Krankenkasse Techniker, Krankenhaus Köln
    /// ...
    /// </summary>
    public class InstitutionType
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized.")]
        public InstitutionType()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }

        [Key(), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public uint Id { get; set; }
        public string Name { get; set; }

        public InstitutionType(string name)
        {
            Name = name;
        }

        [ForeignKey(nameof(Branch))]
        public uint FkBranch { get; set; }

        public virtual InstitutionBranch? Branch { get; set; }
    }
}