﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDatabase.Models.BO
{
    public class HtmlBlock
    {
        [Key]
        public uint Id { get; set; }

        public string Html { get; set; }

        public HtmlBlock(string html)
        {
            Html = html;
        }

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized!")]
        public HtmlBlock()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }
    }
}
