﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace VoteDatabase.Models.BO
{
    public class WebRequest
    {
        public WebRequest(DateTime timestamp, string? identity, IPAddress? remoteIpAddress, string method, string? path, string? query, string? userAgent, string? referer, bool isWebSocket)
        {
            Timestamp = timestamp;
            Identity = identity;
            RemoteIpAddress = remoteIpAddress;
            Method = method;
            Path = path;
            Query = query;
            UserAgent = userAgent;
            Referer = referer;
            IsWebSocket = isWebSocket;
        }

        [Key]
        public uint Id { get; set; }

        public DateTime Timestamp { get; set; }

        public string? Identity { get; set; }

        public IPAddress? RemoteIpAddress { get; set; }

        public string Method { get; set; }

        public string? Path { get; set; }
        public string? Query { get; set; }

        public string? UserAgent { get; set; }

        public string? Referer { get; set; }

        public bool IsWebSocket { get; set; }
    }
}
