﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VoteDatabase.Models.Enums;

namespace VoteDatabase.Models.BO
{
    /// <summary>
    /// Ereignis in einer Behörden-Kommunikation
    /// </summary>
    public class BEvent
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized.")]
        public BEvent()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }

        [Key(), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public uint Id { get; set; }

        /// <summary>
        /// Creation date of this event
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Summary of this event provided by creator
        /// </summary>
        [Column(TypeName = "VARCHAR")]
        [StringLength(1024)]
        public string Summary { get; set; }

        public BEvent(string summary, string text)
        {
            Summary = summary;
            Text = text;
        }

        /// <summary>
        /// Free text detailling this event.
        /// E.g. notes taken during phone call.
        /// </summary>
        /// <remarks>Usually alternative instead of <see cref="AttachmentFile"/></remarks>
        [Column(TypeName = "VARCHAR")]
        [StringLength(10240)]
        public string Text { get; set; }

        /// <summary>
        /// Path to attachment file, e.g. to PDF, audio file, image, ...
        /// </summary>
        public string? AttachmentFile { get; set; }

        [ForeignKey(nameof(Institution))]
        public uint FkInstitution { get; set; }

        [ForeignKey(nameof(BCase))]
        public uint FkBCase { get; set; }
        public virtual BCase? BCase { get; set; }

        /// <summary>
        /// Communication partner involved in this event
        /// </summary>
        public virtual Institution? Institution { get; set; }

        /// <summary>
        /// Type of event (call, letter, ...)
        /// </summary>
        public EventType Type { get; set; }
    }
}
