# Script for creating initial version 0.1 of TeamSlide Cloud Search database

DROP DATABASE IF EXISTS Vote;

CREATE DATABASE Vote;
USE Vote;

# Create database user
CREATE USER 'radaruser'@'localhost' IDENTIFIED BY 'radar_password';
GRANT SELECT, INSERT, DELETE, UPDATE ON CloudSearch.* TO 'radaruser'@'localhost';
