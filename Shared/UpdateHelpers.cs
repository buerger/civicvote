﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace SharedHelpers
{
    public static class UpdateHelpers
    {
        #region Setter methods

        /// <summary>
        /// Sets a string value of an object including check if value is different from current value
        /// and if allowed length is kept.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New string value to use for property</param>
        /// <param name="o">Object which to update with new string value</param>
        /// <param name="selector">Selector for string property in <paramref name="o"/></param>
        /// <param name="modelState">Model state dictionary to which error message is added if value is not valid</param>
        /// <param name="messageReference">Display used for property in potential error message</param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        /// <param name="modelStateErrorKeyPrefix">Prefix of the error key in model state dictionary</param>
        /// <returns>true if value to check was valid and false otherwise</returns>
        public static bool CheckAndSet<T>(string? input, T o, System.Linq.Expressions.Expression<Func<T, string?>> selector,
            ModelStateDictionary modelState, string messageReference,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change, string? modelStateErrorKeyPrefix = null)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            return CheckAndSet(input, o, selector, modelState, messageReference, null, ref change, modelStateErrorKeyPrefix);
        }

        /// <summary>
        /// Sets a string value of an object including check if value is different from current value
        /// and if allowed length is kept.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New string value to use for property</param>
        /// <param name="o">Object which to update with new string value</param>
        /// <param name="selector">Selector for string property in <paramref name="o"/></param>
        /// <param name="modelState">Model state dictionary to which error message is added if value is not valid</param>
        /// <param name="messageReference">Display used for property in potential error message</param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        /// <param name="modelStateErrorKeyPrefix">Prefix of the error key in model state dictionary</param>
        /// <returns>true if value to check was valid and false otherwise</returns>
        public static bool SetIfChanged<T>(string? input, T o, System.Linq.Expressions.Expression<Func<T, string?>> selector,
            ModelStateDictionary modelState, string messageReference,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change, string? modelStateErrorKeyPrefix = null)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            return CheckAndSet(input, o, selector, modelState, messageReference, null, ref change, modelStateErrorKeyPrefix);
        }

        /// <summary>
        /// Sets a string value of an object including check if value is different from current value
        /// and if allowed length is kept.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New string value to use for property</param>
        /// <param name="o">Object which to update with new string value</param>
        /// <param name="selector">Selector for string property in <paramref name="o"/></param>
        /// <param name="modelState">Model state dictionary to which error message is added if value is not valid</param>
        /// <param name="messageReference">Display used for property in potential error message</param>
        /// <param name="additionalCheck">Optional method for an extra check which returns false and a message to show
        /// in case of any problems with the new value</param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        /// <param name="modelStateErrorKeyPrefix">Prefix of the error key in model state dictionary</param>
        /// <returns>true if value to check was valid and false otherwise</returns>
        public static bool CheckAndSet<T>(string? input, T o, System.Linq.Expressions.Expression<Func<T, string?>> selector,
            ModelStateDictionary modelState, string messageReference, Func<string?, Tuple<bool, string>>? additionalCheck,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change, string? modelStateErrorKeyPrefix = null)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }
            if (modelState == null) { throw new ArgumentNullException(nameof(modelState)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }

            if (!EmptyValueAllowed(selector, propInfo, input))
            {
                modelState.AddModelError(propInfo.Name, $"{messageReference} must not be empty");
                return false;
            }

            var currentValue = selector.Compile().Invoke(o);

            if (!Equals(input, currentValue))
            {
                string errorKey = modelStateErrorKeyPrefix?.ToString() + propInfo.Name;
                if (additionalCheck != null)
                {
                    var checkResult = additionalCheck(input);

                    if (!checkResult.Item1)
                    {
                        modelState.AddModelError(errorKey, checkResult.Item2);
                        return false;
                    }
                }

                setMethod.Invoke(o, new object[] { input! });
                change = true;

                if (StringTooLong(o, selector))
                {
                    modelState.AddModelError(errorKey, $"{messageReference} too long");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the length of a string property value is longer than allowed by the MaxLength attribute.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="o">Object</param>
        /// <param name="selector">Selector for string property</param>
        /// <returns>true if current value is longer than allowed</returns>
        public static bool StringTooLong<T>(T o, System.Linq.Expressions.Expression<Func<T, string?>> selector)
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            var memberExpr = selector.Body as System.Linq.Expressions.MemberExpression;
            if (memberExpr == null) { throw new ArgumentException("Invalid expression", nameof(selector)); }

            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression", nameof(selector)); }

            var stringValue = selector.Compile().Invoke(o);

            if (stringValue == null)
            {
                return false;
            }

            object[] attrs = propInfo.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.MaxLengthAttribute), true);
            foreach (object attr in attrs)
            {
                var maxLengthAttr = attr as System.ComponentModel.DataAnnotations.MaxLengthAttribute;
                if (maxLengthAttr != null)
                {
                    return stringValue.Length > maxLengthAttr.Length;
                }
            }

            return false;
        }

        private static bool EmptyValueAllowed<T>(System.Linq.Expressions.Expression<Func<T, string?>> selector, System.Reflection.PropertyInfo propertyInfo, string? value)
        {
            // We cannot use Aploris.Library.Support.StringHelpers.ValueRequired because it is not prepared to work with #nullable
            // It always expects a RequiredAttribute but we are not using it since "string" implies "Required" vs. "string?" which does not

            if (!string.IsNullOrEmpty(value) || IsNullable(propertyInfo))
            {
                return true;
            }

            // Test edge case: string property with Required(AllowEmptyStrings = false)
            bool allowEmptyStrings;
            var valueRequired = ValueRequired(selector, out allowEmptyStrings);
            if (valueRequired && !allowEmptyStrings)
            {
                return false;
            }
            else
            {
                return value != null;
            }
        }

        /// <summary>
        /// Checks if the <see cref="System.ComponentModel.DataAnnotations.RequiredAttribute"/> is set for a property.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="selector">Selector for property</param>
        /// <param name="allowEmptyStrings">If a Required attribute is found its AllowEmptyStrings setting is returned or false otherwise</param>
        /// <returns>true if property value is required</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1021:Avoid out parameters")]
        public static bool ValueRequired<T>(System.Linq.Expressions.Expression<Func<T, string?>> selector, out bool allowEmptyStrings)
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            var memberExpr = selector.Body as System.Linq.Expressions.MemberExpression;
            if (memberExpr == null) { throw new ArgumentException("Invalid member expression", nameof(selector)); }

            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid property expression", nameof(selector)); }

            return ValueRequired<T>(propInfo, out allowEmptyStrings);
        }

        /// <summary>
        /// Checks if the <see cref="System.ComponentModel.DataAnnotations.RequiredAttribute"/> is set for a property.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="propertyInfo">Property to check</param>
        /// <param name="allowEmptyStrings">If a Required attribute is found its AllowEmptyStrings setting is returned or false otherwise</param>
        /// <returns>true if property value is required</returns>
        internal static bool ValueRequired<T>(System.Reflection.PropertyInfo propertyInfo, out bool allowEmptyStrings)
        {
            object[] attrs = propertyInfo.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.RequiredAttribute), true);
            foreach (object attr in attrs)
            {
                var requiredAttr = attr as System.ComponentModel.DataAnnotations.RequiredAttribute;
                if (requiredAttr != null)
                {
                    allowEmptyStrings = requiredAttr.AllowEmptyStrings;
                    return true;
                }
            }

            allowEmptyStrings = false;
            return false;
        }

        private static bool IsNullable(System.Reflection.PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsValueType)
            {
                return Nullable.GetUnderlyingType(propertyInfo.PropertyType) != null;
            }

            var nullable = propertyInfo.CustomAttributes
                .FirstOrDefault(x => x.AttributeType.FullName == "System.Runtime.CompilerServices.NullableAttribute");
            if (nullable != null && nullable.ConstructorArguments.Count == 1)
            {
                var attributeArgument = nullable.ConstructorArguments[0];
                if (attributeArgument.ArgumentType == typeof(byte[]))
                {
                    var args = (System.Collections.ObjectModel.ReadOnlyCollection<System.Reflection.CustomAttributeTypedArgument>)attributeArgument.Value!;
                    if (args.Count > 0 && args[0].ArgumentType == typeof(byte))
                    {
                        return (byte)args[0].Value! == 2;
                    }
                }
                else if (attributeArgument.ArgumentType == typeof(byte))
                {
                    return (byte)attributeArgument.Value! == 2;
                }
            }

            // Compiler might put the NullableAttribute on the class
            for (var type = propertyInfo.DeclaringType; type != null; type = type.DeclaringType)
            {
                var context = type.CustomAttributes
                    .FirstOrDefault(x => x.AttributeType.FullName == "System.Runtime.CompilerServices.NullableContextAttribute");
                if (context != null &&
                    context.ConstructorArguments.Count == 1 &&
                    context.ConstructorArguments[0].ArgumentType == typeof(byte))
                {
                    return (byte)context.ConstructorArguments[0].Value! == 2;
                }
            }

            return false;
        }

        /// <summary>
        /// Sets a Uri value of an object including check if value is different from current value
        /// and if allowed length is kept.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New Uri value to use for property</param>
        /// <param name="o">Object which to update with new Uri value</param>
        /// <param name="selector">Selector for string property in <paramref name="o"/></param>
        /// <param name="modelState">Model state dictionary to which error message is added if value is not valid</param>
        /// <param name="messageReference">Display used for property in potential error message</param>
        /// <param name="requiresAbsoluteUrl">Check if value is absolute URL</param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        /// <returns>true if value to check was valid and false otherwise</returns>
        public static bool CheckAndSet<T>(Uri? input, T o, System.Linq.Expressions.Expression<Func<T, Uri>> selector,
            ModelStateDictionary modelState, string messageReference, bool requiresAbsoluteUrl,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }
            if (modelState == null) { throw new ArgumentNullException(nameof(modelState)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }

            if (input == null)
            {
                modelState.AddModelError(propInfo.Name, $"{messageReference} must not be empty");
                return false;
            }

            if (requiresAbsoluteUrl && !input.IsAbsoluteUri)
            {
                modelState.AddModelError(propInfo.Name, $"{messageReference} must be absolute URL");
                return false;
            }

            var currentValue = selector.Compile().Invoke(o);

            if (!Equals(input, currentValue))
            {
                setMethod.Invoke(o, new object[] { input! });
                change = true;

                var stringValue = input.ToString();

                object[] attrs = propInfo.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.MaxLengthAttribute), true);
                foreach (object attr in attrs)
                {
                    var maxLengthAttr = attr as System.ComponentModel.DataAnnotations.MaxLengthAttribute;
                    if (maxLengthAttr != null)
                    {
                        if (stringValue.Length > maxLengthAttr.Length)
                        {
                            modelState.AddModelError(propInfo.Name, $"{messageReference} too long");
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new boolean value</param>
        /// <param name="selector">Selector for boolean property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(bool input, T o, System.Linq.Expressions.Expression<Func<T, bool>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new nullable boolean value</param>
        /// <param name="selector">Selector for boolean property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(bool? input, T o, System.Linq.Expressions.Expression<Func<T, bool?>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input! });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new integer value</param>
        /// <param name="selector">Selector for integer property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(int input, T o, System.Linq.Expressions.Expression<Func<T, int>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object after conducting check if value is different from current value
        /// and value is ok.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <typeparam name="T2">Type of value to set</typeparam>
        /// <param name="input">New string value to use for property</param>
        /// <param name="o">Object which to update with new string value</param>
        /// <param name="selector">Selector for string property in <paramref name="o"/></param>
        /// <param name="modelState">Model state dictionary to which error message is added if value is not valid</param>
        /// <param name="check">Method for the check which returns false and a message to show
        /// in case of any problems with the new value</param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        /// <param name="valid">Set to false if the new value is invalid, currently only checked for allowed length</param>
        public static void CheckAndSet<T, T2>(T2 input, T o, System.Linq.Expressions.Expression<Func<T, T2>> selector,
            ModelStateDictionary modelState, Func<T2, (bool IsValid, string InvalidMessage)> check,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change, ref bool valid)
#pragma warning restore CA1045 // Do not pass types by reference
            where T2 : struct
        {
            if (check == null) { throw new ArgumentNullException(nameof(check)); }
            CheckAndSetInternal(input, o, selector, modelState, check, ref change, ref valid);
        }

        /// <summary>
        /// Sets a value of an object after conducting check if value is different from current value
        /// and value is ok.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <typeparam name="T2">Type of value to set</typeparam>
        /// <param name="input">New string value to use for property</param>
        /// <param name="o">Object which to update with new string value</param>
        /// <param name="selector">Selector for string property in <paramref name="o"/></param>
        /// <param name="modelState">Model state dictionary to which error message is added if value is not valid</param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T, T2>(T2 input, T o, System.Linq.Expressions.Expression<Func<T, T2>> selector,
            ModelStateDictionary modelState,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
            where T2 : struct
        {
            bool valid = false;
            CheckAndSetInternal(input, o, selector, modelState, null, ref change, ref valid);
        }

        private static void CheckAndSetInternal<T, T2>(T2 input, T o, System.Linq.Expressions.Expression<Func<T, T2>> selector,
            ModelStateDictionary modelState, Func<T2, (bool IsValid, string InvalidMessage)>? check, ref bool change, ref bool valid)
            where T2 : struct
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }
            if (modelState == null) { throw new ArgumentNullException(nameof(modelState)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (!input.Equals(currentValue))
            {
                if (check != null)
                {
                    var checkResult = check(input);
                    if (!checkResult.IsValid)
                    {
                        modelState.AddModelError(propInfo.Name, checkResult.InvalidMessage);
                        valid = false;
                        return;
                    }
                }

                setMethod.Invoke(o, new object[] { input });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new nullable long value</param>
        /// <param name="selector">Selector for integer property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(int? input, T o, System.Linq.Expressions.Expression<Func<T, int?>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input! });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new nullable long value</param>
        /// <param name="selector">Selector for integer property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(long? input, T o, System.Linq.Expressions.Expression<Func<T, long?>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input! });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new integer value</param>
        /// <param name="selector">Selector for integer property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(byte input, T o, System.Linq.Expressions.Expression<Func<T, byte>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new integer value</param>
        /// <param name="selector">Selector for integer property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(short input, T o, System.Linq.Expressions.Expression<Func<T, short>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new decimal value</param>
        /// <param name="selector">Selector for decimal property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(decimal input, T o, System.Linq.Expressions.Expression<Func<T, decimal>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New uint value to use for property</param>
        /// <param name="o">Object which to update with new uint value</param>
        /// <param name="selector">Selector for uint property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(uint input, T o, System.Linq.Expressions.Expression<Func<T, uint>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            if (!(selector.Body is System.Linq.Expressions.MemberExpression memberExpr)) { throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector)); }
            var propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input });
                change = true;
            }
        }

        /// <summary>
        /// Sets a value of an object including check if value is different from current value.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="input">New value to use for property</param>
        /// <param name="o">Object which to update with new nullable date value</param>
        /// <param name="selector">Selector for decimal property in <paramref name="o"/></param>
        /// <param name="change">Set to true if the new value is different from the current value</param>
        public static void SetIfChanged<T>(DateTime? input, T o, System.Linq.Expressions.Expression<Func<T, DateTime?>> selector,
#pragma warning disable CA1045 // Do not pass types by reference
            ref bool change)
#pragma warning restore CA1045 // Do not pass types by reference
        {
            if (selector == null) { throw new ArgumentNullException(nameof(selector)); }

            System.Reflection.PropertyInfo? propInfo;
            if (selector.Body is System.Linq.Expressions.MemberExpression memberExpr)
            {
                propInfo = memberExpr.Member as System.Reflection.PropertyInfo;
            }
            else if (selector.Body is System.Linq.Expressions.UnaryExpression unaryExpr && unaryExpr.NodeType == System.Linq.Expressions.ExpressionType.Convert
                && unaryExpr.Type.Name == "Nullable`1" && unaryExpr.Operand is System.Linq.Expressions.MemberExpression memberExpr2)
            {
                // Passing a non-nullable property will have it wrapped in a convert command
                propInfo = memberExpr2.Member as System.Reflection.PropertyInfo;
            }
            else
            {
                throw new ArgumentException("Invalid expression (MemberExpression)", nameof(selector));
            }

            if (propInfo == null) { throw new ArgumentException("Invalid expression (PropertyInfo)", nameof(selector)); }
            var setMethod = propInfo.GetSetMethod();
            if (setMethod == null) { throw new ArgumentException("Invalid expression (SetMethod)", nameof(selector)); }
            var currentValue = selector.Compile().Invoke(o);

            if (input != currentValue)
            {
                setMethod.Invoke(o, new object[] { input! });
                change = true;
            }
        }

        #endregion
    }
}
