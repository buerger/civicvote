﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vote.Models;
using VoteDatabase.DataAccess;

namespace Vote.Controllers
{
    public class HomeController : Controller
    {
        public static string Name { get; } = nameof(HomeController)[..^10];
        public static string ActionIndex { get; } = nameof(Index);

        private readonly ILogger<HomeController> mLogger;

        public HomeController(ILogger<HomeController> logger, VoteDbContext context)
        {
            if (context == null) { throw new ArgumentNullException(nameof(context)); }
            mLogger = logger;
            foreach (var user in context.User)
            {
                Console.WriteLine(user);
            }
        }

        public IActionResult Index()
        {
            mLogger.LogInformation(nameof(Index));
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
