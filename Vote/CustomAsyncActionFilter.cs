﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Vote
{
    public class CustomAsyncActionFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (next == null) { throw new ArgumentNullException(nameof(next)); }

            //To do : before the action executes
            await next().ConfigureAwait(false);
            //To do : after the action executes
        }
    }
}
