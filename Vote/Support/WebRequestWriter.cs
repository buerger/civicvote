﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Support
{
    public class WebRequestWriter
    {
        private readonly RequestDelegate mNext;

        public WebRequestWriter(RequestDelegate next)
        {
            mNext = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, VoteDbContext dbContext, ILogger<WebRequestWriter> logger,
            Microsoft.Extensions.Options.IOptions<Models.Settings.VoteSettings> settings)
        {
            if (dbContext == null) { throw new ArgumentNullException(nameof(dbContext)); }
            if (httpContext == null) { throw new ArgumentNullException(nameof(httpContext)); }

            try
            {
                if (settings?.Value.EnableRequestLogging != true) { return; }

                var path = httpContext.Request.Path.Value;
                if (path?.TakeLast(6).Contains('.') == true) { return; }

                var identity = httpContext.User?.Identity?.Name;

                var request = new WebRequest(
                    timestamp: DateTime.UtcNow,
                    identity: identity,
                    remoteIpAddress: httpContext.Connection.RemoteIpAddress,
                    method: httpContext.Request.Method,
                    userAgent: httpContext.Request.Headers["User-Agent"],
                    referer: httpContext.Request.Headers["Referrer"],
                    path: path,
                    query: httpContext.Request.QueryString.Value,
                    isWebSocket: httpContext.WebSockets.IsWebSocketRequest);

                dbContext.WebRequest.Add(request);
                await dbContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                logger.LogError($"{nameof(WebRequestWriter)} failed to store request: {ex}");
            }
            finally
            {
                await mNext(httpContext).ConfigureAwait(false);
            }
        }
    }
}
