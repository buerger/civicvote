using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Vote.Areas.State.Pages
{
    public class HeartbeatModel : PageModel
    {
        private readonly ILogger<HeartbeatModel> mLogger;

        public HeartbeatModel(ILogger<HeartbeatModel> logger)
        {
            mLogger = logger;
        }

        public void OnGet()
        {
            if (DateTime.Now.Ticks == 0)
            {
                mLogger.LogInformation("Heartbeat");
                Console.WriteLine("Heartbeat Console");
                System.Diagnostics.Debug.WriteLine("Heartbeat Diagnostic");
            }

            throw new Exception("huch");
        }
    }
}
