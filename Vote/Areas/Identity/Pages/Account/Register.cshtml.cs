﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<User> mSignInManager;
        private readonly UserManager<User> mUserManager;
        private readonly ILogger<RegisterModel> mLogger;
        private readonly IEmailSender mEmailSender;

        public RegisterModel(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            mUserManager = userManager;
            mSignInManager = signInManager;
            mLogger = logger;
            mEmailSender = emailSender;
            Input = new InputModel();
        }

        [BindProperty]
        public InputModel Input { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1056", Justification = "auto-generated")]
        public string? ReturnUrl { get; set; }

        public IList<AuthenticationScheme>? ExternalLogins { get; private set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1034", Justification = "auto-generated")]
        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string? Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string? Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string? ConfirmPassword { get; set; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054", Justification = "auto-generated")]
        public async Task OnGetAsync(string? returnUrl = null)
        {
            if (!Constants.VoteConfig.EnableRegisterPage)
            {
                throw new NotSupportedException();
            }

            ReturnUrl = returnUrl;
            ExternalLogins = (await mSignInManager.GetExternalAuthenticationSchemesAsync().ConfigureAwait(false)).ToList();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054", Justification = "auto-generated")]
        public async Task<IActionResult> OnPostAsync(string? returnUrl = null)
        {
            if (!Constants.VoteConfig.EnableRegisterPage)
            {
                throw new NotSupportedException();
            }

            returnUrl ??= Url.Content("~/");
            ExternalLogins = (await mSignInManager.GetExternalAuthenticationSchemesAsync().ConfigureAwait(false)).ToList();
            if (ModelState.IsValid)
            {
                var user = new User { UserName = Input?.Email, Email = Input?.Email };
                var result = await mUserManager.CreateAsync(user, Input?.Password).ConfigureAwait(false);
                if (result.Succeeded)
                {
                    mLogger.LogInformation("User created a new account with password.");

                    var code = await mUserManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                    await mEmailSender.SendEmailAsync(Input?.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl ?? "#")}'>clicking here</a>.").ConfigureAwait(false);

                    if (mUserManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input?.Email, returnUrl = returnUrl });
                    }
                    else
                    {
                        await mSignInManager.SignInAsync(user, isPersistent: false).ConfigureAwait(false);
                        return LocalRedirect(returnUrl);
                    }
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
