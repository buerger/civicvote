﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Manage.Pages.User
{
    public class CreateModel : PageModel
    {
        private readonly UserManager<VoteDatabase.Models.BO.User> mUserManager;

        public CreateModel(UserManager<VoteDatabase.Models.BO.User> userManager)
        {
            mUserManager = userManager;
            VoteUser = null!;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public VoteDatabase.Models.BO.User VoteUser { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user2 = new VoteDatabase.Models.BO.User { UserName = VoteUser.UserName, Email = VoteUser.Email };
            await mUserManager.CreateAsync(user2).ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
