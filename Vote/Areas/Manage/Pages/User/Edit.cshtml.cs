﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Manage.Pages.User
{
    public class EditModel : PageModel
    {
        private readonly VoteDbContext mContext;
        private readonly UserManager<VoteDatabase.Models.BO.User> mUserManager;

        public EditModel(VoteDatabase.DataAccess.VoteDbContext context, UserManager<VoteDatabase.Models.BO.User> userManager)
        {
            mContext = context;
            mUserManager = userManager;
            VoteUser = null!;
        }

        [BindProperty]
        public VoteDatabase.Models.BO.User VoteUser { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            VoteUser = await mUserManager.FindByIdAsync(id).ConfigureAwait(false);

            if (VoteUser == null)
            {
                return NotFound();
            }

            VoteUser.Roles = await mUserManager.GetRolesAsync(VoteUser).ConfigureAwait(false);
            // empty entry allow user to create new role
            VoteUser.Roles.Add(string.Empty);

            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await mUserManager.FindByIdAsync(VoteUser.Id).ConfigureAwait(false);
            if (user == null)
            {
                return NotFound();
            }

            if (VoteUser.Roles != null)
            {
                user.Roles = await mUserManager.GetRolesAsync(VoteUser).ConfigureAwait(false);
                var newRoles = VoteUser.Roles.Where(r => !user.Roles.Contains(r));
                foreach (var newRole in newRoles)
                {
                    if (!string.IsNullOrWhiteSpace(newRole))
                    {
                        await mUserManager.AddToRoleAsync(user, newRole).ConfigureAwait(false);
                    }
                }

                var removedRoles = user.Roles.Where(r => !VoteUser.Roles!.Contains(r));
                foreach (var removedRole in removedRoles)
                {
                    await mUserManager.RemoveFromRoleAsync(user, removedRole).ConfigureAwait(false);
                }
            }

            bool changed = false;
            SharedHelpers.UpdateHelpers.SetIfChanged(VoteUser.CustomTag2, user, r => r.CustomTag2, ModelState, "CustomTag2", ref changed);

            if (changed)
            {
                await mContext.SaveChangesAsync().ConfigureAwait(false);
            }

            await mUserManager.SetEmailAsync(user, VoteUser.Email).ConfigureAwait(false);
            await mUserManager.SetLockoutEnabledAsync(user, VoteUser.LockoutEnabled).ConfigureAwait(false);
            await mUserManager.SetLockoutEndDateAsync(user, VoteUser.LockoutEnd).ConfigureAwait(false);
            await mUserManager.SetPhoneNumberAsync(user, VoteUser.PhoneNumber).ConfigureAwait(false);
            await mUserManager.SetTwoFactorEnabledAsync(user, VoteUser.TwoFactorEnabled).ConfigureAwait(false);
            await mUserManager.SetUserNameAsync(user, VoteUser.UserName).ConfigureAwait(false);

            await mUserManager.UpdateAsync(user).ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
