﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.InstitutionBranch
{
    public class IndexModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public IndexModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionBranch = null!;
        }

        public IList<VoteDatabase.Models.BO.InstitutionBranch> InstitutionBranch { get; set; }

        public async Task OnGetAsync()
        {
            InstitutionBranch = await mContext.InstitutionBranch.ToListAsync().ConfigureAwait(false);
        }
    }
}
