﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.InstitutionBranch
{
    public class DeleteModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public DeleteModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionBranch = null!;
        }

        [BindProperty]
        public VoteDatabase.Models.BO.InstitutionBranch InstitutionBranch { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            InstitutionBranch = await mContext.InstitutionBranch.FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false);

            if (InstitutionBranch == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            InstitutionBranch = await mContext.InstitutionBranch.FindAsync(id).ConfigureAwait(false);

            if (InstitutionBranch != null)
            {
                mContext.InstitutionBranch.Remove(InstitutionBranch);
                await mContext.SaveChangesAsync().ConfigureAwait(false);
            }

            return RedirectToPage("./Index");
        }
    }
}
