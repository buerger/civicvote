﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.InstitutionBranch
{
    public class EditModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public EditModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionBranch = null!;
        }

        [BindProperty]
        public VoteDatabase.Models.BO.InstitutionBranch InstitutionBranch { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            InstitutionBranch = await mContext.InstitutionBranch.FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false);

            if (InstitutionBranch == null)
            {
                return NotFound();
            }

            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            mContext.Attach(InstitutionBranch).State = EntityState.Modified;

            try
            {
                await mContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InstitutionBranchExists(InstitutionBranch.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool InstitutionBranchExists(uint id)
        {
            return mContext.InstitutionBranch.Any(e => e.Id == id);
        }
    }
}
