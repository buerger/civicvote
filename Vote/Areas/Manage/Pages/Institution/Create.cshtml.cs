﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.Institution
{
    public class CreateModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public CreateModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            Institution = null!;
        }

        public IActionResult OnGet()
        {
            ViewData["FkInstitutionType"] = new SelectList(mContext.InstitutionType, "Id", "Name");
            return Page();
        }

        [BindProperty]
        public VoteDatabase.Models.BO.Institution Institution { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            mContext.Institution.Add(Institution);
            await mContext.SaveChangesAsync().ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
