﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.Institution
{
    public class DeleteModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public DeleteModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            Institution = null!;
        }

        [BindProperty]
        public VoteDatabase.Models.BO.Institution Institution { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Institution = await mContext.Institution
                .Include(i => i.InstitutionType).FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false);

            if (Institution == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Institution = await mContext.Institution.FindAsync(id).ConfigureAwait(false);

            if (Institution != null)
            {
                mContext.Institution.Remove(Institution);
                await mContext.SaveChangesAsync().ConfigureAwait(false);
            }

            return RedirectToPage("./Index");
        }
    }
}
