﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.Institution
{
    public class IndexModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public IndexModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            Institution = null!;
        }

        public IList<VoteDatabase.Models.BO.Institution> Institution { get; set; }

        public async Task OnGetAsync()
        {
            Institution = await mContext.Institution
                .Include(i => i.InstitutionType).ToListAsync().ConfigureAwait(false);
        }
    }
}
