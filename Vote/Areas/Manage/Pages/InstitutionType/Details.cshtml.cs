﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.InstitutionType
{
    public class DetailsModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public DetailsModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionType = null!;
        }

        public VoteDatabase.Models.BO.InstitutionType InstitutionType { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            InstitutionType = await mContext.InstitutionType
                .Include(i => i.Branch).FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false);

            if (InstitutionType == null)
            {
                return NotFound();
            }

            return Page();
        }
    }
}
