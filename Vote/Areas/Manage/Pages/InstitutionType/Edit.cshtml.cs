﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.InstitutionType
{
    public class EditModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public EditModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionType = null!;
        }

        [BindProperty]
        public VoteDatabase.Models.BO.InstitutionType InstitutionType { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            InstitutionType = await mContext.InstitutionType
                .Include(i => i.Branch).FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false);

            if (InstitutionType == null)
            {
                return NotFound();
            }

            ViewData["FkBranch"] = new SelectList(mContext.InstitutionBranch, "Id", "Name");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            mContext.Attach(InstitutionType).State = EntityState.Modified;

            try
            {
                await mContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InstitutionTypeExists(InstitutionType.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool InstitutionTypeExists(uint id)
        {
            return mContext.InstitutionType.Any(e => e.Id == id);
        }
    }
}
