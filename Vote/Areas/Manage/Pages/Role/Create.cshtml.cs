using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.Role
{
    public class CreateModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;
        private readonly RoleManager<IdentityRole> mRoleManager;

        public CreateModel(VoteDbContext context, RoleManager<IdentityRole> roleManager)
        {
            mContext = context;
            mRoleManager = roleManager;
            Role = null!;
        }

        public IActionResult OnGet()
        {
            //ViewData["FkRoleType"] = new SelectList(mContext.RoleType, "Id", "Name");
            return Page();
        }

        [BindProperty]
        public IdentityRole<string> Role { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await mRoleManager.CreateAsync(new Microsoft.AspNetCore.Identity.IdentityRole(Role.Name)).ConfigureAwait(false);
            await mContext.SaveChangesAsync().ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
