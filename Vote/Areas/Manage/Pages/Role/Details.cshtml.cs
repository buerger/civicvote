using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.Role
{
    public class DetailsModel : PageModel
    {
        private readonly RoleManager<IdentityRole> mRoleManager;

        public DetailsModel(RoleManager<IdentityRole> roleManager)
        {
            Role = null!;
            mRoleManager = roleManager;
        }

        public IdentityRole Role { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Role = await mRoleManager.FindByIdAsync(id).ConfigureAwait(false);

            if (Role == null)
            {
                return NotFound();
            }

            return Page();
        }
    }
}
