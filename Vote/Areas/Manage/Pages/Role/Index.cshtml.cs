using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Areas.Edit.Pages.Role
{
    public class IndexModel : PageModel
    {
        private readonly RoleManager<IdentityRole> mRoleManager;

        public IndexModel(RoleManager<IdentityRole> roleManager)
        {
            mRoleManager = roleManager;
            Role = null!;
        }

        public IList<IdentityRole> Role { get; set; }

        public async Task OnGetAsync()
        {
            Role = await mRoleManager.Roles.ToListAsync().ConfigureAwait(false);
        }
    }
}
