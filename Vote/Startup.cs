using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Piranha;
using Vote.Constants;
using Vote.Controllers;
using Vote.Models;
using Vote.Models.Settings;
using Vote.Support;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;
using Roles = VoteDatabase.Constants.Roles;

namespace Vote
{
#pragma warning disable CA1506
    public class Startup
#pragma warning restore CA1506
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // https://www.puresourcecode.com/dotnet/net-core/using-an-in-memory-repository-keys-will-not-be-persisted-to-storage-asp-net-core-under-iis/
            services.AddDataProtection().PersistKeysToFileSystem(new System.IO.DirectoryInfo(System.IO.Path.Combine(Program.DataAndLogsFolderProvider.Root, "Keys")));

            var voteSettings = ConfigureDatabaseAndSettings(services);

            services.AddControllersWithViews();

            var connectionStringCms = new MySql.Data.MySqlClient.MySqlConnectionStringBuilder
            {
                Server = voteSettings.DatabaseConnectionCms.Server,
                Port = voteSettings.DatabaseConnectionCms.Port,
                Database = voteSettings.DatabaseConnectionCms.Database,
                Password = voteSettings.DatabaseConnectionCms.Password,
                UserID = voteSettings.DatabaseConnectionCms.Username,
            }.ConnectionString;

            // Razor pages folder is explicitly used by Piranha. Other pages should go to dedicate Razor Area.
            services.AddRazorPages(options =>
            {
                // Require login for /Vote/*
                options.Conventions.AuthorizeAreaFolder("Vote", "/");

                // Require "Manage" permission for /Manage/*
                options.Conventions.AuthorizeAreaFolder("Manage", "/", PermissionPolicy.Manage);
            }).WithRazorPagesRoot("/PagesCms");
            AddPiranha(services, connectionStringCms);

            ConfigureAuthorizationAndAuthentication(services);

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
            });
        }

        private static void ConfigureAuthorizationAndAuthentication(IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PermissionPolicy.Manage, policy => policy.RequireRole(Roles.Administrator));

                // Override Piranha policies after AddPiranha()
                foreach (var policy in Piranha.Manager.Permission.All())
                {
                    // Grant all Piranha policies to Roles.PiranhaAdministrator
                    options.AddPolicy(policy, policy => policy.RequireRole(Roles.PiranhaAdministrator));
                }

                foreach (var permission in Piranha.Security.Permission.All())
                {
                    // Grant all Piranha permissions to Roles.PiranhaAdministrator
                    options.AddPolicy(permission, policy => policy.RequireRole(Roles.PiranhaAdministrator));
                }
            });

            // https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/additional-claims?view=aspnetcore-6.0
            services.AddAuthentication();

            // TODO: Add password-less authentication via email
            // https://andrewlock.net/implementing-custom-token-providers-for-passwordless-authentication-in-asp-net-core-identity/
        }

        private VoteSettings ConfigureDatabaseAndSettings(IServiceCollection services)
        {
            var voteSettings = new VoteSettings();
            Configuration.Bind(voteSettings);
            SharedHelpers.RecursiveValidator.Validate(voteSettings);
            services.Configure<VoteSettings>(Configuration);

            var connectionStringVote = new MySql.Data.MySqlClient.MySqlConnectionStringBuilder
            {
                Server = voteSettings.DatabaseConnectionVote.Server,
                Port = voteSettings.DatabaseConnectionVote.Port,
                Database = voteSettings.DatabaseConnectionVote.Database,
                Password = voteSettings.DatabaseConnectionVote.Password,
                UserID = voteSettings.DatabaseConnectionVote.Username,
            }.ConnectionString;

            services.AddDbContextPool<VoteDbContext>(builder =>
            {
                try
                {
                    builder.UseMySql(
                        connectionStringVote,
                        ServerVersion.AutoDetect(connectionStringVote),
                        options =>
                        {
                            options.SchemaBehavior(Pomelo.EntityFrameworkCore.MySql.Infrastructure.MySqlSchemaBehavior.Translate,
                                (schema, entity) => $"{schema}{(schema == null ? null : ".")}{entity}");
                            options.EnableRetryOnFailure();
                        });
                }
                catch (Exception ex)
                {
                    throw new Exception($"Failed to access database. Please fix configuration '{nameof(voteSettings.DatabaseConnectionVote)}'.", ex);
                }
            });

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<User>(options =>
            {
                options.SignIn.RequireConfirmedAccount = true;
                options.Password.RequireNonAlphanumeric = true;
            }).AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<VoteDbContext>();

            return voteSettings;
        }

        private static void AddPiranha(IServiceCollection services, string connectionString)
        {
            // Service setup
            services.AddPiranha(options =>
            {
                options.UseFileStorage(naming: Piranha.Local.FileStorageNaming.UniqueFolderNames);
                options.UseImageSharp();
                options.UseManager();
                options.UseTinyMCE();
                options.UseMemoryCache();

                options.UseEF<Piranha.Data.EF.MySql.MySqlDb>(db =>
                    db.UseMySql(
                        connectionString,
                        ServerVersion.AutoDetect(connectionString),
                        options =>
                        {
                            options.EnableRetryOnFailure();
                        }));

                options.LoginUrl = Areas.UrlPaths.Login;
                options.UseStartpageRouting = true;
                options.UsePageRouting = true;
            });

            // basePath is relative to /httpdocs
            // Store uploads in folder next to httpdocs_data (outside of httpdocs)
            services.AddPiranhaFileStorage(basePath: "../uploads/", baseUrl: "~/uploads/", naming: Piranha.Local.FileStorageNaming.UniqueFolderNames);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
#pragma warning disable CA1506 // Avoid excessive class coupling
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
#pragma warning restore CA1506 // Avoid excessive class coupling
            Piranha.IApi api)
        {
            // Cache-Control must be define early!
            app.UseStaticFiles(new StaticFileOptions()
            {
                HttpsCompression = Microsoft.AspNetCore.Http.Features.HttpsCompressionMode.Compress,
                OnPrepareResponse = (context) =>
                {
                    var headers = context.Context.Response.GetTypedHeaders();
                    headers.CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue
                    {
                        Public = true,
                        MaxAge = TimeSpan.FromDays(30),
                    };
                },
            });

            // Create static file provider for uploads folders
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.GetFullPath("..\\uploads")),
                RequestPath = "/uploads",
                OnPrepareResponse = (context) =>
                {
                    var headers = context.Context.Response.GetTypedHeaders();
                    headers.CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue
                    {
                        Public = true,
                        MaxAge = TimeSpan.FromDays(30),
                    };
                },
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
                //app.UseStatusCodePages();
            }
            else
            {
                app.UseExceptionHandler(Areas.UrlPaths.Error);
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCookiePolicy();

            app.UseMiddleware<WebRequestWriter>();

            // First configure Piranha, then set up authorization (otherwise CMS not accessible)
            ConfigurePiranha(app, api);

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=" + HomeController.Name + "}/{action=" + HomeController.ActionIndex + "}/{id?}");
                endpoints.MapRazorPages();
            });
        }

        private static void ConfigurePiranha(IApplicationBuilder app, IApi api)
        {
            // Initialize Piranha
            Piranha.App.Init(api);

            // Configure cache level
            App.CacheLevel = Piranha.Cache.CacheLevel.Full;

            // Register custom components
            App.Blocks.Register<Models.Cms.Blocks.FormBlock>();
            App.Blocks.Register<Models.Cms.Blocks.ButtonBlock>();
            App.Blocks.Register<Models.Cms.Blocks.UploadBlock>();
            App.Blocks.Register<Models.Cms.Blocks.Header1Block>();
            App.Blocks.Register<Models.Cms.Blocks.Header2Block>();

            // Build content types
            new Piranha.AttributeBuilder.ContentTypeBuilder(api)
                .AddAssembly(typeof(Startup).Assembly)
                .Build()
                .DeleteOrphans();

            // Configure Tiny MCE
            Piranha.Manager.Editor.EditorConfig.FromFile(Constants.VoteConfig.TinyMceEditorConfigFilePath);

            // Middle-ware setup
            app.UsePiranha(options =>
            {
                options.UseManager();
                options.UseTinyMCE();
                //options.UseIdentity();
            });
            app.UsePiranhaManager();
        }
    }
}
