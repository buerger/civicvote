using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Serilog;
using Vote.Constants;
using VoteDatabase.DataAccess;

namespace Vote
{
    public static class Program
    {
        internal static Exception? StartupException { get; set; }
        internal static PhysicalFileProvider DataAndLogsFolderProvider { get; } = new PhysicalFileProvider(Path.GetFullPath(VoteConfig.DataAndLogsFolder));

        public async static Task Main(string[] args)
        {
            try
            {
                CheckConfigurationFiles();

                var host = CreateHostBuilder(args).Build();

                await MigrateDatabase(host).ConfigureAwait(false);

                host.Run();
            }
            catch (Exception ex)
            {
                StartupException = ex;
                ExceptionCreateHostBuilder().Build().Run();
            }
        }

        private static void CheckConfigurationFiles()
        {
            if (!Directory.Exists(VoteConfig.DataAndLogsFolder))
            {
                Directory.CreateDirectory(VoteConfig.DataAndLogsFolder);
            }

            if (!File.Exists(VoteConfig.AppSettingsFilePath))
            {
                throw new Exception($"Please create settings file {Path.GetFullPath(VoteConfig.AppSettingsFilePath)} from {VoteConfig.AppSettingsTemplateFile}.");
            }

            if (!File.Exists(VoteConfig.TinyMceEditorConfigFilePath))
            {
                if (!File.Exists(VoteConfig.TinyMceEditorConfigFileSourcePath))
                {
                    throw new Exception($"Editor configuration file {VoteConfig.TinyMceEditorConfigFileSourcePath} does not exist. Please re-deploy.");
                }
                else
                {
                    File.Move(VoteConfig.TinyMceEditorConfigFileSourcePath, VoteConfig.TinyMceEditorConfigFilePath);
                }
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile(DataAndLogsFolderProvider.Root + VoteConfig.AppSettingsFile)
               .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config.AddJsonFile(DataAndLogsFolderProvider, VoteConfig.AppSettingsFile, optional: false, reloadOnChange: false);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog();
        }

        public static IHostBuilder ExceptionCreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<ExceptionStartup>();
                });
        }

        private async static Task MigrateDatabase(IHost host)
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            var context = services.GetRequiredService<VoteDbContext>();

            var assemblyMigrations = context.Database.GetMigrations();
            var appliedMigrations = context.Database.GetAppliedMigrations();
            var createDatabase = !appliedMigrations.Any();
            var pendingMigrations = context.Database.GetPendingMigrations();

            var unknownMigrations = appliedMigrations.Except(assemblyMigrations);
            if (unknownMigrations.Any())
            {
                SharedHelpers.Debug.Break();

                throw new Exception($"Application version too old. " +
                    $"Unknown applied migration(s):{string.Join(",", unknownMigrations)}. " +
                    $"Last assembly migration:{assemblyMigrations.LastOrDefault() ?? "not found"}.");
            }
            else if (pendingMigrations.Any())
            {
                context.Database.Migrate();
                if (createDatabase)
                {
                    var settings = services.GetRequiredService<Microsoft.Extensions.Options.IOptions<Models.Settings.VoteSettings>>();
                    var user = await context.User.SingleAsync(u => u.UserName == "first@user.com").ConfigureAwait(false);

                    // Initializing and assigning custom roles
                    var roleManager = services.GetRequiredService<Microsoft.AspNetCore.Identity.RoleManager<Microsoft.AspNetCore.Identity.IdentityRole>>();
                    var userManager = services.GetRequiredService<Microsoft.AspNetCore.Identity.UserManager<VoteDatabase.Models.BO.User>>();

                    await CreateRoleAndAddUser(roleManager, userManager, VoteDatabase.Constants.Roles.Administrator, user).ConfigureAwait(false);
                    await CreateRoleAndAddUser(roleManager, userManager, VoteDatabase.Constants.Roles.PiranhaAdministrator, user).ConfigureAwait(false);

                    user.PasswordHash = userManager.PasswordHasher.HashPassword(user, settings.Value.InitialAdminPassword);
                    await context.SaveChangesAsync().ConfigureAwait(false);
                }
            }
        }

        private static async Task CreateRoleAndAddUser(Microsoft.AspNetCore.Identity.RoleManager<Microsoft.AspNetCore.Identity.IdentityRole> roleManager, Microsoft.AspNetCore.Identity.UserManager<VoteDatabase.Models.BO.User> userManager, string roleName, VoteDatabase.Models.BO.User user)
        {
            await roleManager.CreateAsync(new Microsoft.AspNetCore.Identity.IdentityRole(roleName)).ConfigureAwait(false);
            await userManager.AddToRoleAsync(user, roleName).ConfigureAwait(false);
        }
    }
}
