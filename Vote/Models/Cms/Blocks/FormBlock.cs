﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Piranha.Extend;
using Piranha.Extend.Fields;
using Piranha.Models;

namespace Vote.Models.Cms.Blocks
{
    /// <summary>
    /// Single column quote block.
    /// </summary>
    [BlockGroupType(Name = "Form", Category = "Html forms", Icon = "fas fa-th-large", Display = BlockDisplayMode.Vertical)]
    public class FormBlock : BlockGroup, ISearchable
    {
        /// <summary>
        /// Name of upload element
        /// </summary>
        [Field(Title = "Action")]
        public StringField UploadAction { get; set; } = null!;

        /// <summary>
        /// Gets the content that should be indexed for searching.
        /// </summary>
        /// <returns>One item per line</returns>
        public string GetIndexedContent()
        {
            var content = new StringBuilder();

            foreach (var item in Items)
            {
                if (item is ISearchable searchItem)
                {
                    var value = searchItem.GetIndexedContent();

                    if (!string.IsNullOrEmpty(value))
                    {
                        content.AppendLine(value);
                    }
                }
            }

            return content.ToString();
        }
    }
}
