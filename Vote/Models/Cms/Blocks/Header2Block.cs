﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Piranha.Extend;
using Piranha.Extend.Blocks;
using Piranha.Extend.Fields;
using Piranha.Extend.Fields.Settings;

namespace Vote.Models.Cms.Blocks
{
    [BlockType(Name = "H2 Header", Category = "Html forms", Icon = "fas fa-heading", IsGeneric = true)]
    public class Header2Block : Block
    {
        /// <summary>
        /// Name of upload element
        /// </summary>
        [Field(Title = "H2 Header text")]
        public StringField HeaderText { get; set; } = null!;
    }
}
