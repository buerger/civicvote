﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Vote.Models.Settings
{
    public class VoteSettings
    {
        public const string ErrorMessageDefaultValue = "\"{0}\" should be set via default value if not set in '" + Constants.VoteConfig.AppSettingsFilePath + "'";
        public const string ErrorMessageRequiredValue = "Please define \"{0}\" in '" + Constants.VoteConfig.AppSettingsFilePath + "'";

        /// <summary>
        /// Connection details for accessing database for CivicVote
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public MySqlSettings DatabaseConnectionVote { get; set; } = null!;

        /// <summary>
        /// Connection details for accessing database for CivicVote's integrated Piranha CMS
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public MySqlSettings DatabaseConnectionCms { get; set; } = null!;

        /// <summary>
        /// Initial admin password. It is set for user "first@user.com" during initializing the database.
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public string InitialAdminPassword { get; set; } = null!;

        /// <summary>
        /// If true, each web request is logged to database table WebRequest.
        /// </summary>
        public bool EnableRequestLogging { get; set; }
    }
}
