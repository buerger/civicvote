﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Vote.Models.Settings
{
    public class MySqlSettings
    {
        [Required(ErrorMessage = VoteSettings.ErrorMessageRequiredValue)]
        public string Server { get; set; } = null!;

        [Required(ErrorMessage = VoteSettings.ErrorMessageRequiredValue)]
        public uint Port { get; set; }

        [Required(ErrorMessage = VoteSettings.ErrorMessageRequiredValue)]
        public string Username { get; set; } = null!;

        [Required(ErrorMessage = VoteSettings.ErrorMessageRequiredValue)]
        public string Password { get; set; } = null!;

        [Required(ErrorMessage = VoteSettings.ErrorMessageRequiredValue)]
        public string Database { get; set; } = null!;
    }
}
