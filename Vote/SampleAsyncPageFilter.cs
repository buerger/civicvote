﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Policy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Vote
{
    public class SampleAsyncPageFilter : IAsyncPageFilter
    {
        private readonly IConfiguration mConfig;
        //private readonly IAuthorizationPolicyProvider mPolicyProvider;
        //private readonly IPolicyEvaluator mPolicyEvaluator;

        public SampleAsyncPageFilter(IConfiguration config)
        //, IAuthorizationPolicyProvider policyProvider,
        //IPolicyEvaluator policyEvaluator)
        {
            mConfig = config;
            //mPolicyProvider = policyProvider;
            //mPolicyEvaluator = policyEvaluator;
        }

        public async Task OnPageHandlerSelectionAsync(PageHandlerSelectedContext context)
        {
            if (context == null) { throw new ArgumentNullException(nameof(context)); }

            var key = mConfig["UserAgentID"];
            context.HttpContext.Request.Headers.TryGetValue("user-agent",
                                                            out StringValues value);
            //ProcessUserAgent.Write(context.ActionDescriptor.DisplayName,
            //                       "SampleAsyncPageFilter.OnPageHandlerSelectionAsync",
            //                       value, key.ToString());
            Console.WriteLine(value);
            Console.WriteLine(key);

            if (context.ActionDescriptor.AreaName == "Manage")
            {
            }

            var attribute = context.HandlerMethod?.MethodInfo?.GetCustomAttribute<AuthorizePageHandlerAttribute>();
            if (attribute is null)
            {
                return;
            }

            await Task.Delay(0).ConfigureAwait(false);

            var httpContext = context.HttpContext;

            //AuthorizationPolicy policy = new AuthorizationPolicy(new List);
            var policyBuilder = new AuthorizationPolicyBuilder("authScheme");
            //policy.AddRequirements(new MinimumAgeRequirement(age));
            var policy = policyBuilder.Build();

            if (policy.AuthenticationSchemes.Count > 0)
            {
                foreach (var scheme in policy.AuthenticationSchemes)
                {
                    await httpContext.ForbidAsync(scheme).ConfigureAwait(false);
                }
            }
            else
            {
                await httpContext.ForbidAsync().ConfigureAwait(false);
            }

            //httpContext.ForbidAsync

            //var policy = await AuthorizationPolicy.CombineAsync(mPolicyProvider, new[] { attribute }).ConfigureAwait(false);
            //if (policy is null)
            //{
            //    return;
            //}

            //await AuthorizeAsync(context, policy).ConfigureAwait(false);
        }

        //#region AuthZ - do not change
        //private async Task AuthorizeAsync(ActionContext actionContext, AuthorizationPolicy policy)
        //{
        //    var httpContext = actionContext.HttpContext;
        //    var authenticateResult = await mPolicyEvaluator.AuthenticateAsync(policy, httpContext).ConfigureAwait(false);
        //    var authorizeResult = await mPolicyEvaluator.AuthorizeAsync(policy, authenticateResult, httpContext, actionContext.ActionDescriptor).ConfigureAwait(false);
        //    if (authorizeResult.Challenged)
        //    {
        //        if (policy.AuthenticationSchemes.Count > 0)
        //        {
        //            foreach (var scheme in policy.AuthenticationSchemes)
        //            {
        //                await httpContext.ChallengeAsync(scheme).ConfigureAwait(false);
        //            }
        //        }
        //        else
        //        {
        //            await httpContext.ChallengeAsync().ConfigureAwait(false);
        //        }

        //        return;
        //    }
        //    else if (authorizeResult.Forbidden)
        //    {
        //        if (policy.AuthenticationSchemes.Count > 0)
        //        {
        //            foreach (var scheme in policy.AuthenticationSchemes)
        //            {
        //                await httpContext.ForbidAsync(scheme).ConfigureAwait(false);
        //            }
        //        }
        //        else
        //        {
        //            await httpContext.ForbidAsync().ConfigureAwait(false);
        //        }

        //        return;
        //    }
        //}
        //#endregion

        public async Task OnPageHandlerExecutionAsync(PageHandlerExecutingContext context,
                                                      PageHandlerExecutionDelegate next)
        {
            if (next == null) { throw new ArgumentNullException(nameof(next)); }

            // Do post work.
            await next.Invoke().ConfigureAwait(false);
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
#pragma warning disable SA1402 // File may only contain a single type
    public sealed class AuthorizePageHandlerAttribute : Attribute, IAuthorizeData
#pragma warning restore SA1402 // File may only contain a single type
    {
        public AuthorizePageHandlerAttribute(string? policy = null)
        {
            Policy = policy;
        }

#pragma warning disable CA1019 // Define accessors for attribute arguments -- public setter required for IAuthorizeData
        public string? Policy { get; set; }
#pragma warning restore CA1019 // Define accessors for attribute arguments

        public string? Roles { get; set; }

        public string? AuthenticationSchemes { get; set; }
    }
}