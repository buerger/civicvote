﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vote.Constants
{
    static public class PermissionPolicy
    {
        public const string Manage = "ManageArea";

        public static string[] All()
        {
            return new[]
            {
                Manage,
            };
        }
    }
}
