﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vote.Constants
{
    public static class Names
    {
        /// <summary>
        /// Internal name of web application. Use for logging only.
        /// </summary>
        internal const string AppName = "CivicVote";

        /// <summary>
        /// URL that shows exception log if application started in ExceptionStartup mode.
        /// </summary>
        internal const string ExceptionViewUrl = "/oops";
    }
}
